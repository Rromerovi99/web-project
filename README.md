# Maven - Java Web Application Project
Es un projecto web que despliega la fecha actual.

Se construyo con  Maven 3, Spring 5 MVC, JUnit 5, Logback y Jetty como servidor web.


## 1. ¿Como correr el proyecto?

### 1.1 Para ejecutarlo con Jetty.
Ejecutar los siguientes comandos:

```sh
 cd web-project 
 mvn jetty:run
```

Una vez que Jetty esté levantado, abir un navegador y escribir:

```sh
 http://localhost:8080
```

### 1.2 Para crear el archivo WAR para su deployment:
Ejecutar los siguientes comandos:

```sh
 cd web-project 
 mvn package or mvn war:war
```
El archivo WAR se crea en 'target/{finalName}'

```sh
 - Ver el archivo pom.xml
```